//
//  TicTacToeApp.swift
//  TicTacToe
//
//  Created by Ilya Lisovski on 19.10.22.
//

import SwiftUI

@main
struct TicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
